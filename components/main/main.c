#include <string.h>
#include <unistd.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#include "proxyos/core/core.h"
#include "proxyos/core/device_tree.h"
#include "proxyos/core/message.h"
#include "proxyos/core/module.h"
#include "proxyos/core/peripheral.h"
#include "proxyos/core/storage.h"
#include "proxyos/core/system.h"

#include "proxyos/drivers/drivers.h"
#include "proxyos/drivers/serial.h"

static const char* TAG = "main";

static void
send_discover_reply(peripheral_t const* const* peripherals, char* message) {
  char* it = message + sizeof(uint32_t);
  message_write_header(it, "peripheral_list", &it);

  // peripherals
  {
    bson_set_element_type(it, BSON_ARRAY, &it);
    bson_set_element_name(it, "peripherals", strlen("peripherals"), &it);
    char* peripherals_begin = it;
    it += sizeof(uint32_t);

    char buffer[16]                       = {0};
    int index                             = 0;
    peripheral_t const* const* peripheral = peripherals;
    while (*peripheral) {
      snprintf(buffer, sizeof(buffer), "%d", index);

      bson_set_element_type(it, BSON_OBJECT, &it);
      bson_set_element_name(it, buffer, strlen(buffer), &it);
      char* obj = it;
      it += sizeof(uint32_t);
      {
        peripheral_declare(*peripheral, it, &it);
        (*peripheral)->declaration(*peripheral, it, &it);
      }
      bson_set_element_type(it, BSON_END, &it);
      bson_set_size(obj, it - obj, NULL);

      ++index;
      ++peripheral;
    }

    bson_set_element_type(it, BSON_END, &it);
    bson_set_size(peripherals_begin, it - peripherals_begin, NULL);
  }

  bson_set_element_type(it, BSON_END, &it);
  bson_set_size(message, it - message, NULL);

  LOGBSOND(TAG, message);

  message_send(message, it - message);
}

static void
start_all(peripheral_t* const* peripherals) {
  LOGD(TAG, "Starting all peripherals");
  peripheral_t* const* peripheral = peripherals;
  while (*peripheral) {
    if ((*peripheral)->start) (*peripheral)->start(*peripheral);
    ++peripheral;
  }
}

static void
stop_all(peripheral_t* const* peripherals) {
  LOGD(TAG, "Stopping all peripherals");
  peripheral_t* const* peripheral = peripherals;
  while (*peripheral) {
    if ((*peripheral)->stop) (*peripheral)->stop(*peripheral);
    ++peripheral;
  }
}

void
app_main(void) {
  proxyos_core_init();
  proxyos_drivers_init();

  module_status();

  char const* device_tree = BSON_EMPTY;

  char* storage_device_tree = NULL;
  {
    size_t size = 0;
    int ret     = storage_get_size("device_tree", &size);
    if (ret != 0) {
      LOGW(TAG, "Unable to get device_tree from NVS");
      goto abort_storage_dt;
    }

    storage_device_tree = malloc(size);

    ret = storage_get("device_tree", storage_device_tree, size);
    if (ret != 0) {
      LOGW(TAG, "Unable to get device_tree from NVS");
      goto abort_storage_dt;
    }

    LOGI(TAG, "Device tree successfully loaded from NVS");
  }

abort_storage_dt:
  if (storage_device_tree) { device_tree = storage_device_tree; }

  peripheral_t* const* const peripherals = device_tree_load(device_tree);
  device_tree_status();

  if (storage_device_tree) {
    free(storage_device_tree);
    device_tree         = NULL;
    storage_device_tree = NULL;
  }

  // Channel
  serial_init();
  message_set_send_callback(serial_write_bytes);

  uint32_t message_max_size = 10 * 1024;
  char* message             = malloc(message_max_size);
  if (!message) {
    while (true) {
      LOGE(TAG, "Failed to malloc message");
      sleep(1);
    }
  }

  uint32_t* const message_size = (uint32_t*) message;
  while (true) {
    int const ret = serial_read_bytes((char*) message_size, sizeof(*message_size), 1000);
    if (ret != 4) {
      LOGD(TAG, "Failed to get message size");
      continue;
    }

    if (*message_size > message_max_size) {
      LOGE(TAG, "Receiving too big message (size=%lu)", (long unsigned) *message_size);
      serial_purge();
      continue;
    }

    serial_read_bytes(message + sizeof(uint32_t), *message_size - sizeof(uint32_t), 1000);
    char const* it = message + sizeof(uint32_t);

    //bson_print(message, 0, 2);
    //printf("\n");

    char const* type = NULL;
    {
      bson_element_t tp = bson_get_element_type(it, &it);
      if (tp != BSON_STRING) {
        LOGE(TAG, "Invalid element type: %d. Should be: %d", (int) tp, (int) BSON_STRING);
        serial_purge();
        continue;
      }

      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      if (strcmp(name, "tp") != 0) {
        LOGE(TAG, "Invalid element name: '%s'. Should be 'tp'", name);
        serial_purge();
        continue;
      }

      uint32_t type_size;
      type = bson_get_element_value_string(it, &type_size, &it);
      LOGD(TAG, "Received message of type '%s'", type);
    }

    /****/ if (strcmp(type, "discover") == 0) {
      LOGI(TAG, "Got 'discover' request");
      send_discover_reply((peripheral_t const* const*) peripherals, message);
    } else if (strcmp(type, "start_all") == 0) {
      LOGI(TAG, "Got 'start_all' request");
      start_all(peripherals);
    } else if (strcmp(type, "stop_all") == 0) {
      LOGI(TAG, "Got 'stop_all' request");
      stop_all(peripherals);
    } else if (strcmp(type, "restart") == 0) {
      LOGI(TAG, "Got 'restart' request");
      system_restart();
    } else if (strcmp(type, "set_device_tree") == 0) {
      LOGI(TAG, "Got 'set_device_tree' request");

      bson_element_t tp = bson_get_element_type(it, &it);
      if (tp != BSON_OBJECT) {
        LOGE(TAG, "Invalid element type: %d. Should be: %d", (int) tp, (int) BSON_OBJECT);
        serial_purge();
        continue;
      }

      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      if (strcmp(name, "device_tree") != 0) {
        LOGE(TAG, "Invalid element name: '%s'. Should be 'device_tree'", name);
        serial_purge();
        continue;
      }

      LOGBSOND(TAG, it);

      uint32_t size = bson_get_size(it, NULL);
      storage_set("device_tree", it, size);
    } else {
      int32_t peripheral_id;
      {
        bson_element_t tp = bson_get_element_type(it, &it);
        if (tp != BSON_INT32) {
          LOGE(TAG, "Invalid element type: %d. Should be: %d", (int) tp, (int) BSON_INT32);
          serial_purge();
          continue;
        }

        uint32_t name_size;
        char const* name = bson_get_element_name(it, &name_size, &it);

        if (strcmp(name, "pr") != 0) {
          LOGE(TAG, "Invalid element name: '%s'. Should be 'pr'", name);
          serial_purge();
          continue;
        }

        peripheral_id = bson_get_element_value_int32(it, &it);
      }

      peripheral_t* const* peripheral = peripherals;
      while (*peripheral) {
        if ((*peripheral)->id == peripheral_id) {
          if ((*peripheral)->callback)
            (*peripheral)->callback(*peripheral, type, it);
          else {
            LOGW(
                TAG, "Peripheral '%s' received a message but has no callback", (*peripheral)->name);
          }

          break;
        }

        ++peripheral;
      }
    }
  }
}
