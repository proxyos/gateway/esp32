#!/bin/bash

THIS="$(readlink -f "${BASH_SOURCE[0]}")"
TOOLS_DIR="$(dirname "$THIS")"

BIN="$1"
TRACE=("$@")
TRACE_FILE="/tmp/trace.txt"

echo "${TRACE[@]}" > "$TRACE_FILE"
java -jar "$TOOLS_DIR/EspStackTraceDecoder.jar" "$(which xtensa-esp32-elf-addr2line)" "$BIN" "$TRACE_FILE"
